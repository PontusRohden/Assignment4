The 4th assignment for Experis Academy 2021.

The Assignment:
Build  a  dynamic  webpage  using  “vanilla”  JavaScript.  Follow  the  guidelines  given  below,  but  feel  free  to  
customize the app if you want.


Page content:
Laptops:
    Here you can select the different laptops in store, read the description and specs, view the price and buy the laptop.

The Bank:
    Displays your current balance and gives you the option to get a loan so you can afford a more expensive laptop.

Work:
    This is were you make money to buy you dream laptop. Just press the "work"-button then transfer for earned pay to the bank with the "bank"-button. 


Link to the webpage hosted on Github Pages: https://pontusrohdenexperis.github.io/ 
